var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Skeema tietokantamerkinnöille
var MeasurementsSchema = new Schema({
 location: String,
 temperature: Number,
 timestamp: Date
});

MeasurementsSchema.query.byCity = function(city) {
    return this.find({ location: city }).sort({ timestamp: -1 });
};

//minimimerkintä 24h ajalta
MeasurementsSchema.query.dayMinLastWeek = function(city) {
    return this.find({ location: city, timestamp: { $gte: new Date(new Date().getTime()-24*60*60*1000).toISOString() }}).sort({temperature: 1}).limit(1);
}

//Maksimimerkintä 24h ajalta
MeasurementsSchema.query.dayMaxLastWeek = function(city) {
    return this.find({ location: city, timestamp: { $gte: new Date(new Date().getTime()-24*60*60*1000).toISOString() }}).sort({ temperature: -1 }).limit(1);
}

module.exports = mongoose.model('Measurement', MeasurementsSchema);