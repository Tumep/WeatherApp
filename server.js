var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var serve = require('serve');

var config = require('./config');
var Measurement = require('./model/measurements');

//db connection
    mongoose.connect(config.connectString);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB connection error:'));
db.once('open', function() {
  console.log("DB connection successful");
});

//and create our instances
var app = express();
var router = express.Router();

//set our port to either a predetermined port number if you have set 
//it up, or 3001
var port = process.env.API_PORT || 3001;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//CORS virheiden esto
app.use(function(req, res, next) {
 res.setHeader('Access-Control-Allow-Origin', '*');
 res.setHeader('Access-Control-Allow-Credentials', 'true');
 res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
 res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

//Ei cachea
 res.setHeader('Cache-Control', 'no-cache');
 next();
});

//now we can set the route path & initialize the API
router.get('/', function(req, res) {
 res.json({ message: 'API Initialized!'});
});

router.route('/measurements').get(function(req, res) {
    Measurement.find(function(err, measurements) {
        if(err)
        res.send(err);
        res.json(measurements)
    }).sort({ timestamp: -1 });
}).post(function(req, res) {
    var measurement = new Measurement();
    measurement.location = req.body.location;
    measurement.temperature = req.body.temperature;
    measurement.timestamp = Date.now();

    measurement.save(function(err) {
        if (err)
        res.send(err);
        res.json({ message: 'Measurement successfully added!'});
    });
});

router.route('/measurements/:city').get(function(req, res) {
    Measurement.find( function(err, measurements) {
        if(err)
        res.send(err);
        res.json(measurements)
    }).byCity(req.params.city);
});

router.route('/measurements/:city/min').get(function(req, res) {
    Measurement.find( function(err, measurements) {
        if(err)
        res.send(err);
        res.json(measurements)
    }).dayMinLastWeek(req.params.city);
});

router.route('/measurements/:city/max').get(function(req, res) {
    Measurement.find( function(err, measurements) {
        if(err)
        res.send(err);
        res.json(measurements)
    }).dayMaxLastWeek(req.params.city);
});

//Use our router configuration when we call /api
app.use('/api', router);

//starts the server and listens for requests
app.listen(port, function() {
 console.log('api running on port ' + port);
});


//Remove for development environment
const client = serve(__dirname + '/build', {
    port: 3000,
    single: true
  });