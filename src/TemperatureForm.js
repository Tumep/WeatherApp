import React, { Component } from 'react';
import style from './style';

class TemperatureForm extends Component {
 constructor(props) {
    super(props);
    this.state = { temperature: '', location: '' };
    this.handleTemperatureChange = this.handleTemperatureChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
 }
 handleTemperatureChange(e) {
    this.setState({ temperature: e.target.value });
 }
 handleLocationChange(e) {
    this.setState({ location: e.target.value });
 }
 handleSubmit(e) {
    e.preventDefault();
    let temperature = this.state.temperature.trim();
    let location = this.state.location.trim();
    if (!temperature || !location ) {
        return;
 }
    this.props.onMeasurementSubmit({ temperature: temperature, location: location});
    this.setState({temperature: '', location: ''});
 }
 render() {
    return (
        <form style={ style.temperatureForm } onSubmit={ this.handleSubmit }>
            <h3 style={ style.temperatureFormHeader }>Log a temperature:</h3>
            <input required type='number' min='-40' max='40' step='0.01' placeholder='Temperature'
            style={ style.temperatureFormTemperature } value={ this.state.temperature } 
            onChange={ this.handleTemperatureChange } />
            <select required style={ style.temperatureFormLocation} value={ this.state.location } 
            onChange={ this.handleLocationChange }>
                <option disabled value=''>Location</option>
                <option value='Tokio'>Tokio</option>
                <option value='Helsinki'>Helsinki</option>
                <option value='New York'>New York</option>
                <option value='Amsterdam'>Amsterdam</option>
                <option value='Dubai'>Dubai</option>
            </select>
            <input type='submit' style={ style.temperatureFormPost } value='Post' />
        </form>
    )
 }
}

export default TemperatureForm;
