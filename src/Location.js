import React, { Component } from "react"
import { Row, Col } from 'react-grid-system'
import style from './style'

class Location extends Component {
    render() {
      return (
        <div style={style.location}>
          <Row>
            <Col sm={12}>
            <h3>{this.props.name}</h3>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <p>Latest temperature</p>
            </Col>
            <Col sm={6}>
              <p id="temperature">{this.props.currentTemp}</p>
            </Col>
            <Col sm={6}>
              <p>Lowest today</p>
            </Col>
            <Col sm={6}>
              <p id="temperature">{this.props.minTemp}</p>
            </Col>
            <Col sm={6}>
              <p>Highest today</p>
            </Col>
            <Col sm={6}>
              <p id="temperature">{this.props.maxTemp}</p>
            </Col>
          </Row>
        </div>
      )
    }
  }
  
  export default Location
