import React, {Component} from 'react'
import axios from 'axios'
import {Container, Row, Col } from 'react-grid-system'

import TemperatureForm from './TemperatureForm'
import Location from './Location'
import TemperatureList from './TemperatureList';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      cities: [{
        name: "Tokio",
        coordinates: [139.7328635, 35.6584421],
        currentTemp: 0,
        minTemp: 0,
        maxTemp: 0
      }, {
        name: "Helsinki",
        coordinates: [24.9490830, 60.1697530],
        currentTemp: 0,
        minTemp: 0,
        maxTemp: 0
      }, {
        name: "New York",
        coordinates: [-73.9938438, 40.7406905],
        currentTemp: 0,
        minTemp: 0,
        maxTemp: 0
      }, {
        name: "Amsterdam",
        coordinates: [4.9040238, 52.3650691],
        currentTemp: 0,
        minTemp: 0,
        maxTemp: 0
      }, {
        name: "Dubai",
        coordinates: [55.1562243, 25.092535],
        currentTemp: 0,
        minTemp: 0,
        maxTemp: 0
      }]
    };
    this.loadTemperaturesFromServer = this.loadTemperaturesFromServer.bind(this);
    this.handleMeasurementSubmit = this.handleMeasurementSubmit.bind(this);
    this.updateTemperatures = this.updateTemperatures.bind(this);
    }
    //Load initial list of data and also child component data
    loadTemperaturesFromServer() {
      axios.get(this.props.url)
      .then(res => {
      this.setState({ data: res.data });
      });
      for (var j = 0; j < this.state.cities.length; j++){
        const city = this.state.cities[j].name;
        //Api calls, Axios all wouldn't work :/
        axios.get(this.props.url + '/' + city).then(res => {
          if(typeof(res.data[0]) !== 'undefined') {
            this.updateTemperatures(city, {currentTemp: res.data[0].temperature + ' °C'});
          } else {
            this.updateTemperatures(city, {currentTemp: 'No temperature logged yet'});
          }
        });
        axios.get(this.props.url + '/' + city + '/min').then(res => {
          if(typeof(res.data[0]) !== 'undefined') {
            this.updateTemperatures(city, {minTemp: res.data[0].temperature + ' °C'});
          } else {
            this.updateTemperatures(city, {minTemp: 'No measurements in last 24h'});
          }
        });
        axios.get(this.props.url + '/' + city + '/max').then(res => {
          if(typeof(res.data[0]) !== 'undefined') {
            this.updateTemperatures(city, {maxTemp: res.data[0].temperature + ' °C'});
          } else {
            this.updateTemperatures(city, {maxTemp: 'No measurements in last 24h'});
          }
        });
      }
    }
    //Use city and custom attributes to update the initial state with data from API
    updateTemperatures(city, attributes) {
      var index = this.state.cities.findIndex(x=> x.name === city);
      if (index === -1)
        console.log("error");
      else
        this.setState({
          cities: [
             ...this.state.cities.slice(0,index),
             Object.assign({}, this.state.cities[index], attributes),
             ...this.state.cities.slice(index+1)
          ]
        });
    }
    //Handle temperature submit from form
    handleMeasurementSubmit(measurement) {
      let temperatures = this.state.data;
      this.setState({ data: temperatures });
      axios.post(this.props.url, measurement)
      .then(res => {
        //Not the correct way of operating (creates overhead) but works (tm), fix this
        this.loadTemperaturesFromServer();
      })
      .catch(err => {
        console.error(err);
      })
    }
    //Initial data load on mount
    componentDidMount() {
      this.loadTemperaturesFromServer();
      setInterval(this.loadTemperaturesFromServer, this.props.pollInterval);
    }
  render() {
    let locationData = this.state.cities.map(function(data, idx) {
      return (
      <Col key={idx} sm={12} md={4} lg={3}>
        <Location key={idx} name={data.name} currentTemp={data.currentTemp} minTemp={data.minTemp} maxTemp={data.maxTemp} />
      </Col>
      )
    })
    return (
      <Container>
        <TemperatureForm onMeasurementSubmit = {this.handleMeasurementSubmit} />
        <Row>
          {locationData}
        </Row>
        <Row>
          <Col sm={12}>
          <h2>Latest temperatures</h2>
            <TemperatureList data={this.state.data} />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App
