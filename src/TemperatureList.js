import React, { Component } from 'react'
import Temperature from './Temperature'
import style from './style'

class TemperatureList extends Component {
    render() {
        let temperatureNodes = this.props.data.map(temperature => {
            return (
                <Temperature location={ temperature.location } key={ temperature._id } 
                    date={ temperature.timestamp } temperature={ temperature.temperature } separator=' | '>
                </Temperature>
            )
        })
        return (
            <div style={ style.temperatureList }>
                { temperatureNodes }
            </div>
        )
        }
    }


 
export default TemperatureList;