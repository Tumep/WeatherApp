import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { unregister } from './registerServiceWorker';

ReactDOM.render(<App url = 'http://87.92.111.123:3001/api/measurements' pollInterval={10000} />, document.getElementById('root'));
unregister();
