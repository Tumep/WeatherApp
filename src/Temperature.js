
import React, { Component } from 'react';
import style from './style';
import Moment from 'react-moment'

class Temperature extends Component {
 render() {
    return (
    <div style={ style.temperature }>
        <p>
            {this.props.location}
            {this.props.separator}
            <Moment date={this.props.date} format="DD.MM.YYYY  HH:MM" />
            {this.props.separator}
            {this.props.temperature + " °C"}
        </p>
    </div>
    )
 }
}

export default Temperature;