const style = {
    location: {
        border:'1px solid #f1f1f1',
        borderRadius: '25px',
        padding: '0 12px',
        marginBottom: '5px'
    },
    temperatureList: {
        border:'1px solid #f1f1f1',
        padding:'0 12px',
        maxHeight:'70vh',
        overflow:'scroll'
    },
    temperatureForm: {
        display:'flex',
        flexFlow: 'row wrap',
        justifyContent:'center',
        alignItems:'baseline'
    },
    temperatureFormHeader: {
        marginRight:'10px'
    },
    temperatureFormTemperature: {
        minWidth:'150px',
        margin:'3px',
        padding:'0 10px',
        borderRadius:'3px',
        height:'40px'
    },
    temperatureFormLocation: {
        minWidth:'150px',
        margin:'3px',
        padding:'0 10px',
        borderRadius:'3px',
        height:'40px'
    },
    temperatureFormPost: {
        minWidth:'75px',
        height:'40px',
        margin:'5px 3px',
        fontSize:'1rem',
        backgroundColor:'#A3CDFD',
        borderRadius:'3px',
        color:'#fff',
        textTransform:'uppercase',
        letterSpacing:'.055rem',
        border:'none'
    }
}

module.exports = style;
