# Tehtävänanto ja ratkaisu

Ilmastotieteilijät tarvitsevat uuden web-sovelluksen, jolla voivat seurata säätilan muutoksia ympäri maailmaa.

Saimme heiltä seuraavan toivelistan:

- Web-sovellukseen ei tarvitse kirjautua sisään
- Kaikki käyttäjät voivat syöttää lämpötilahavaintoja havaintopisteiltä, joita on 5 kappaletta (katso lista tehtävänannon lopussa)
- Uusia havaintopisteitä ei tarvitse pystyä lisäämään web-sovelluksen kautta
- Kirjatut havainnot ovat kaikkien käyttäjien katseltavissa
- Havainnoista tärkeimmät tiedot ovat:
  - Tämänhetkinen lämpötila jokaisessa havaintopisteessä tuoreimman kirjatun havainnon mukaan
  - Ylin ja alin lämpötila viimeisen 24 tunnin ajalta jokaisessa havaintopisteessä
- On tutkimuksen kannalta tärkeää, että tallennettavat havainnot ovat valideja
- On myös tärkeää, että tiedot pysyvät tallessa, vaikka sovelluksesi kaatuisi

## Ohjeita tehtävän tekemiseen

- Voit olettaa, että tutkijoiden käytössä on moderni web-selain (esimerkiksi Chrome tai Firefox)
- Kiinnitä huomiota siihen, että koodisi on jaettu järkeviin ja helposti luettaviin kokonaisuuksiin
- Sovellusta olisi hyvä päästä testaamaan: voit siis laittaa valmiin sovelluksen pyörimään vaikkapa [Herokuun](https://www.heroku.com/)
- Voit toimittaa kesätyötehtävän lähdekoodit meille osoitteeseen summerjob@reaktor.com esimerkiksi linkkinä GitHub-repositorioon, tai ZIP-tiedostona. Liitä mukaan kaikki tehtävän kannalta olennaiset tiedostot.

## Havaintopisteet

```json
Tokio: 35.6584421,139.7328635
Helsinki: 60.1697530,24.9490830
New York: 40.7406905,-73.9938438
Amsterdam: 52.3650691,4.9040238
Dubai: 25.092535,55.1562243
```

---

## Ratkaisu

Valitsin ratkaisuksi ongelmaan React.js kirjaston, koska se vaikutti mielenkiintoiselta ja sopivalta tällaisen web-sovelluksen nopeaan toteutukseen. Tueksi tiedon pysyvyyden varmistamiseksi lisäsin mukaan tietokannan ja APIn tiedon syötön ja lukemisen mahdollistamiseksi. Ensimmäisissä toteutuksissa käytin erillistä Node.js palvelinta, jonka korvasin suoraan reactiin ympätyllä Express-"palvelimella".

## Suunnitellut osat

- Lämpötilansyöttökaavake (Form)
- Havaintopisteet (1 per paikka)
- Kaikki havainnot (List)
- Palvelin (API)
- Tietokanta (Mongodb)

## Komponentit

- Client (päämoduuli: App.js)
  - React (To enable react :) )
  - Axios (Api calls)
  - React-grid-system (simppeli container - col - row rakenne)
  - moment + react-moment (ajan parsimiseen tietokantamuodosta)
- Server
  - express (API- ja reititysfunktiot)
  - mongoose (Integraatio MongoDb:n kanssa)
  - body-parser (JSON datan käsittely)
  - nodemon (automaattinen serverin uudelleenkäynnistys muutoksia (development))
  - foreman (Api+client start-dev scripti procfilen avulla)
  - serve (Tuotantopalvelimen pystytykseen)

## Ominaisuudet

- Lämpötilan syöttö
  - Syötteen tarkistus
  - Syötteen tallennus tietokantaan
- Lämpötilan lukeminen
  - Luetun lämpötilan näyttäminen
    - Listaa kaikki havainnot
    - Paikkakuntakohtaiset havainnot
      - Nykyinen lämpötila (viimeisin havainto)
      - Suurin lämpötila viimeisen 24 tunnin ajalta
      - Pienin lämpötila viimeisen 24 tunnin ajalta

## Käyttö

### Development

- lisää config.js tiedosto, johon sisällöksi:

```javascript
var config =  {
        connectString: 'INSERT_MONGODB_CONNECTION_STRING_HERE'
    };
```

- Kommentoi server.js tiedostosta serve lopusta (Merkitty kommenteilla)

```terminal
npm install
npm run start-dev
```

### Production

```terminal
npm install
npm run build
serve -s build -p portti
```

## TODO ja huomiot

- Maailmankartta visualisoimaan pisteitä (Ei toteutettu, react-simple-maps testattu toimivaksi)
- Visuaalinen ilme vaatisi parannusta
- Grid system voisi korvata flexboxilla
- Sovelluksen turvallisuuteen ei ole kiinnitetty huomiota